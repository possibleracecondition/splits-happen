package org.gitlab.possibleracecondition;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * <p>
 * Given valid sequences of rolls for one line of American Ten-Pin Bowling on standard in, produces
 * the total score for each game as lines on standard out.
 * </p>
 * <p>
 * Can be run with argument {@code [-b [buffer size]]} to enable output buffering for greatly
 * improved performance. Buffer defaults to 8192 chars if no size is provided.
 * </p>
 */
public class SplitsHappen {
  public static void main(String[] args) {
    boolean bufferOutput = false;
    int bufferSize = 8192;

    // Parse command line arguments
    if (args.length > 0) {
      if (args[0].equals("-b")) { // enable output buffering
        bufferOutput = true;
        if (args.length == 2) {
          bufferSize = Integer.parseInt(args[1]); // custom buffer size
        }
      }
    }

    // Buffer output for performance
    try (PrintWriter out = new PrintWriter(
        new BufferedWriter(new OutputStreamWriter(System.out, StandardCharsets.ISO_8859_1),
            bufferSize),
        !bufferOutput)) {

      // Flush output buffer on exit
      Runtime.getRuntime().addShutdownHook(new Thread(() -> {
        out.flush();
      }));

      /*
       * Read lines from standard in, using the ISO_8859_1 Charset for decoding since all characters
       * are ASCII and it's faster than the US_ASCII decoder. Convert each line to a character array
       * to allow random access to its contents, and pass along to a function to calculate the game
       * score. Finally, print out each score on a separate line.
       */
      new BufferedReader(new InputStreamReader(System.in, StandardCharsets.ISO_8859_1)).lines()
          .map(String::toCharArray).map(SplitsHappen::calcGameScore).forEachOrdered(out::println);
    }
  }

  /**
   * Given a valid sequence of rolls for one line of American Ten-Pin Bowling, produces the total
   * score for the game.
   * 
   * @param rollSequence a valid sequence of rolls for one line of American Ten-Pin Bowling
   * @return the total score for the game
   */
  public static int calcGameScore(char[] rollSequence) {
    int score = 0;

    /*
     * Loop through each roll to calculate the score, keeping track of (half-)frames as we go
     */
    for (int i = 0, halfFrames = 0; halfFrames++ < 20; i++) {
      char roll = rollSequence[i];
      switch (roll) {
        case 'X': { // strike, score for the frame is 10 + the sum knocked down in next 2 rolls
          /*
           * Strike takes up a full frame, advance halfFrame counter
           */
          halfFrames++;
          char nextRoll = rollSequence[i + 1];

          /*
           * Subtracting '0' from the given character converts ASCII character digits to the literal
           * integers they represent. Taking the max of them and 0 ensures that dash characters
           * ('-') become 0.
           */
          int nextRollScore = nextRoll == 'X' ? 10 : Math.max(nextRoll - '0', 0);

          char nextNextRoll = rollSequence[i + 2];
          int nextNextRollScore = nextNextRoll == 'X' ? 10
              : nextNextRoll == '/' ? 10 - nextRollScore : Math.max(nextNextRoll - '0', 0);

          score += 10 + nextRollScore + nextNextRollScore;
          break;
        }
        case '/': { // spare, score for the frame is 10 + number of pins knocked down in next throw
          char previousRoll = rollSequence[i - 1];

          char nextRoll = rollSequence[i + 1];
          int nextRollScore = nextRoll == 'X' ? 10 : Math.max(nextRoll - '0', 0);

          if (previousRoll != '-') {
            /*
             * If the previous roll wasn't zero, subtracting it is required since it is already
             * accounted for in the score.
             */
            score += 10 - Math.max(previousRoll - '0', 0) + nextRollScore;
          } else {
            score += 10 + nextRollScore;
          }
          break;
        }
        case '-': // Dashes mean 0 pins, don't waste time with them.
          break;
        default: { // Nothing special happens, add the number of knocked down pins to the score
          score += roll - '0';
        }
      }
    }

    return score;
  }
}
